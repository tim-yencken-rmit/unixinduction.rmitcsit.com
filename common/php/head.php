<!-- head - base -->

        <!-- base -->
        <?php $base = '/'; ?>

        <!-- ascii
        <?php require_once('common/ascii/watermark'); ?>
        -->

        <!-- title -->
        <title>Unix Induction -- <?php echo $page; ?></title>

        <!-- meta -->
        <meta charset="utf-8">
        <meta name="author"         content="RMIT CSIT">
        <meta name="description"    content="A resource for students to get a basic understanding of UNIX and LINUX and its role at RMIT.">

        <!-- css -->
        <link rel="stylesheet" href="<?php echo $base; ?>common/css/style.css">

        <!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


<!-- end - head - base -->
