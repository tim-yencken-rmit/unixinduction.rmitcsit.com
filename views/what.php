<?php

    /** globals **/
    $page = 'Welcome';

?>
<html>
    <head>
        <?php

            /** load -- head -- common **/
            require_once('common/php/head.php');

        ?>
    </head>
    <body class='bg-body'>
        <div id='what' class='menu'>

            <!-- what - spacer -->
            <div class='nav-spacer-animation' id='spacer'></div>

            <!-- what - links -->
            <br><br>
            <div id='links'>
                <ul>
                    <li class='bg-link-1 selected'>
                        <a class='color-link' href='../what/'>
                            <h2>What</h2>
                            <p class='hidden'>What is a UNIX induction.</p>
                        </a>
                    </li>
                    <li class='bg-link-1'>
                        <a class='color-link' href='../why/'>
                            <h2>Why</h2>
                            <p class='hidden'>Why is a UNIX induction important?</p>
                        </a>
                    </li>
                    <li class='bg-link-2'>
                        <a class='color-link' href='../start/'>
                            <h2>Start</h2>
                            <p class='hidden'>Start induction.</p>
                        </a>
                    </li>
                </ul>
                <!-- <h3><a class='color-rmit' href='http://rmitcsit.com/'>RMIT-CSIT</a></h3> -->
            </div>
            <br><br>

            <!-- what - information -->
            <div id='information' class='bg-title'>
                <p>A UNIX induction is a set of resources designed to get you orientated and comfortable using a <a href='http://en.wikipedia.org/wiki/Unix'>UNIX</a> / <a href='http://en.wikipedia.org/wiki/Unix-like'>UNIX-LIKE</a> operating system.</p>
                <p>To complete this induction you must gain enough understanding to be able to perform basic tasks via a Unix or Unix like <a href='http://en.wikipedia.org/wiki/Command-line_interface'>command line interface</a> and proficiency with at least one command line editor.</p>
                <p>To accomplish this we have provided a series of online resources. It is encouraged you try all the material as it will help you decide what works best for you.</p>
                <p>It is your responsibility to ensure you complete this induction.</p>
            </div>

        </div>
    </body>
</html>
