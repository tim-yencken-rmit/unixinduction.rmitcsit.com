<?php

    /** globals **/
    $page = 'Welcome';

?>
<html>
    <head>
        <?php

            /** load -- head -- common **/
            require_once('common/php/head.php');

        ?>
    </head>
    <body class='bg-body'>
        <div id='welcome' class='menu'>

            <!-- welcome - spacer -->
            <div class='welcome-spacer-animation' id='spacer'></div>

            <!-- welcome - links -->
            <div id='links'>
                <h1>Unix Induction</h1>
                <br>
                <ul>
                    <li class='bg-link-1'>
                        <a class='color-link' href='../what/'>
                            <h2>What</h2>
                            <p class='hidden'>What is a UNIX induction?</p>
                        </a>
                    </li>
                    <li class='bg-link-1'>
                        <a class='color-link' href='../why/'>
                            <h2>Why</h2>
                            <p class='hidden'>Why is a UNIX induction important?</p>
                        </a>
                    </li>
                    <li class='bg-link-2'>
                        <a class='color-link' href='../start/'>
                            <h2>Start</h2>
                            <p>Start Induction</p>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </body>
</html>
