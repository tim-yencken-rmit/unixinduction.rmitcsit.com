<?php

    /** globals **/
    $page = 'Start';

?>
<html>
    <head>
        <?php

            /** load -- head -- common **/
            require_once('common/php/head.php');

        ?>
    </head>
    <body class='bg-body'>
        <div id='start' class='menu'>

            <!-- start - spacer -->
            <div class='welcome-spacer-animation' id='spacer'></div>

            <!-- start - links -->
            <div id='links'>
                <h1>Start Unix Induction</h1>
                <h2>Select Induction Type</h2>
                <br><br>
                <ul>
                    <li class='bg-link-1'>
                        <a class='color-link' href='../induction/web/'>
                            <h2>HTML5</h2>
                            <p class='hidden'>An HTML5 web based UNIX induction <strong>assembled by Tim Yencken</strong></p>
                            <p class='hidden'><strong>Please ensure you have an up to date browser</strong></p>
                        </a>
                    </li>
                    <li class='bg-link-1'>
                        <a class='color-link' href='../induction/pdf/induction%20guide%20by%20paul%20miller.pdf'>
                            <h2>PDF</h2>
                            <p class='hidden'>A PDF based UNIX induction <strong>created by Paul Miller</strong></p>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </body>
</html>
