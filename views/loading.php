<?php

    /** globals **/
    $page = 'Loading';

?>
<html>
    <head>
        <link rel="stylesheet" href="../common/css/loading.css">
        <?php

            /** load -- head -- common **/
            require_once('common/php/head.php');

        ?>
    </head>
    <body class='bg_body'>
        <div id='loading'>

            <!-- top -->
            <div id='top' class='loading-panel-animation' >
                <div id='title' class='loading-title-top'>
                    <!-- <h1 class='text-heading'>Loading</h1> -->
                </div>
            </div>

            <!-- bar -->
            <div id='bar' class='loading-bar-animation'>

            </div>

            <!-- bottom -->
            <div id='bot' class='loading-panel-animation'>
                <div id='title' class='loading-title-bot'>
                    <a class='text-heading'         href='<?php echo $referer; ?>'  >ready</a>
                </div>
            </div>

        </div>
    </body>
</html>
