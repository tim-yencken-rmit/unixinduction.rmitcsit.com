<?php

    /** globals **/
    $page = 'Welcome';

?>
<html>
    <head>
        <?php

            /** load -- head -- common **/
            require_once('common/php/head.php');

        ?>
    </head>
    <body class='bg-body'>
        <div id='why' class='menu'>

            <!-- why - spacer -->
            <div class='nav-spacer-animation' id='spacer'></div>

            <!-- why - links -->
            <br><br>
            <div id='links'>
                <ul>
                    <li class='bg-link-1'>
                        <a class='color-link' href='../what/'>
                            <h2>What</h2>
                            <p class='hidden'>What is a UNIX induction.</p>
                        </a>
                    </li>
                    <li class='bg-link-1 selected'>
                        <a class='color-link' href='../why/'>
                            <h2>Why</h2>
                            <p class='hidden'>Why is a UNIX induction important.</p>
                        </a>
                    </li>
                    <li class='bg-link-2'>
                        <a class='color-link' href='../start/'>
                            <h2>Start</h2>
                            <p class='hidden'>Start Induction</p>
                        </a>
                    </li>
                </ul>
                <!-- <h3><a class='color-rmit' href='http://rmitcsit.com/'>RMIT-CSIT</a></h3> -->
            </div>
            <br><br>

            <!-- why - information -->
            <div id='information' class='bg-title'>
                <p>A <a href='http://en.wikipedia.org/wiki/Unix'>UNIX</a> induction is important as it introduces you to tools that are essential to any CSIT graduate.</p>
                <p>Your courses assume you have completed a UNIX induction and that you posses at the very least an entry level understanding of <a href='http://en.wikipedia.org/wiki/Unix'>UNIX</a> & the <a href='http://en.wikipedia.org/wiki/Command-line_interface'>command line</a>.</p>
                <p><a href='http://en.wikipedia.org/wiki/Unix'>UNIX</a> & <a href='http://en.wikipedia.org/wiki/Unix-like'>UNIX-LIKE</a> operating systems are the backbone of modern computing.</p>
                <p>An understanding of <a href='http://en.wikipedia.org/wiki/Unix'>UNIX</a> separates users from power users and helps you unlock the true power of your computer and or any computer.</p>
            </div>

        </div>
    </body>
</html>
