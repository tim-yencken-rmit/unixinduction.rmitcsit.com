<?php




    /** Sessions **/
    session_start();

    /** Required **/
    require_once 'vendor/autoload.php';

    /** Globals **/
    $slim_routes_ignore = array('.', '..');

    /** SLiM **/

        /** SLiM -- options **/
        $options = array();
        $options['templates.path']  = './views';

        /** SLiM -- init **/
        $SLiM = new \Slim\Slim($options);

                /** SLiM -- load routes **/
                $routes = scandir('routes/');
        foreach($routes as $route) {
            if( ! in_array($route, $slim_routes_ignore)) {
                require_once('routes/'.$route);
            }
        }

        /** SLiM -- run **/
        $SLiM->run();

    /** END -- SLiM **/




?>
