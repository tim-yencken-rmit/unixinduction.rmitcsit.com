
	$(document).ready(function() {

        // transition
        $(document).on("click",".link",function(e){
            console.log('start function');
            var pageCurrentID   = "#" + $(this).attr("data-current-page");
            var pageNextID      = "#" + $(this).attr("data-next-page");
            var pageNextHREF    = "pages/" + $(this).attr("data-next-page") + ".php";
            var pageNextGet     = "?page=" + $(this).attr("data-next-page");

            if ($(pageNextID).length > 0) {
                $(pageCurrentID).toggleClass("hidden-y");
                $(pageNextID).toggleClass("hidden-y");
            } else {
                $(pageCurrentID).toggleClass("hidden-y");
                $(pageNextID).toggleClass("hidden-y");
            }

            history.pushState(null, null, pageNextGet)

        });

        // terms
        $(document).on("click",".term",function(e){
            $(".text", this).toggleClass("text-hide");
        });

        // on page loads
        $("#text").toggleClass("load");

	});
