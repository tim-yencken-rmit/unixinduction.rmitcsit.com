

    <!-- welcome -->
    <div id='welcome' class="page hidden-y transition-10">
        <div id='content' class="bg-red">
            <div id='text' class=''>
                <span>RMIT UNIX INDUCTION</span>
                <br>
                <span>HTML5</span>
                <br>
                <span id='forward' class='link button button-2 hover-bg-green-2' data-current-page='welcome' data-next-page='intro'>
                    <i class="fa fa-linux"></i> Start Induction
                </span>
            </div>
        </div>
    </div>
    <!-- end - welcome -->


