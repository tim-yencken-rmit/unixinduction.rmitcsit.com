

    <!-- introduction -->
    <div id='intro' class="page hidden-y transition-10">

        <!-- introduction - back -->
        <div id='back' class="link button bg-green hover-bg-red transition-5" data-current-page='intro' data-next-page='welcome'>
            <i class="fa fa-sort-up"></i>
        </div> 

        <!-- introduction - content -->
        <div id='content' class="bg-green">
            <div id='welcome'>
                <h1><i class='fa fa-qq'></i></h1>
                <h2>Introduction</h2>
                <p>Welcome to the RMIT CSIT Unix induction, HTML5 version.</p>
                <p>This induction is designed to get you orientated and comfortable using a UNIX / UNIX-LIKE operating system.</p>
                <p>The goal of this induction is to help you set up a UNIX or UNIX-LIKE operating and give you an entry level understanding of the TERMINAL and various common command line tools.</p>
                <p>In the next section you will find a list of terminology that it is important to know before commencing the induction.</p>
                <p>This induction will take ~2-4 hours to complete, and it is encouraged to start this when rested and relaxed.</p>
                <p>Please follow the links and read everything, until comfortable with the content.</p>
                <p>Remember it is your responsibility to ensure you understand the content presented in this induction.</p>
            </div>
        </div>

        <!-- introduction - forward -->
        <div id='forward' class="link button bg-green hover-bg-blue transition-5" data-current-page='intro' data-next-page='terminology'>
            <i class="fa fa-list"></i>
        </div>

    </div>
    <!-- end - introduction -->


