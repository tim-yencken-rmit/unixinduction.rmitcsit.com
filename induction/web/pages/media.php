
    <!-- media -->
    <div id='media' class="page hidden-y transition-10">
        <div id='back' class="link button bg-off-white hover-bg-green transition-5" data-current-page='media' data-next-page='start'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-off-white">
            <div id='container'>

                <!-- media - title -->
                <h1>Getting **nix</h1>

                <!-- media - info -->
                <div id='info'>
                    <br>
                    <p>In this section you will select your desired UNIX or UNIX-LIKE operating system, prepare the boot media and if successful boot into your new UNIX / UNIX-LIKE enviroment.</p>
                    <br>
                    <p>There are many UNIX or UNIX-LIKE operating systems, each with their own benefits and weaknesses. There is no single answer to every situation. It is important for you to try as many different operating systems as possible. Never stop exploring, from now until the end of time.</p>
                    <br>
                    <p>On the next page you will be presented with a hand picked list of recommended UNIX or UNIX-LIKE operating systems. You are free to use any you like.</p>
                    <br>
                    <p>After selecting your desired operating system, you will need to create your bootable media.</p>
                    <p>This entails copying your downloaded operating system to your desired media ( USB, DVD, CD, etc ).</p>
                    <p>There are a variety of tools to assist you in this process. Instructions provided on upcoming pages.</p>
                    <br>
                    <p>Once your boot media is prepared, you can use it on most computers to start your UNIX / UNIX-LIKE enviroment.</p>
                    <p>Most UNIX or UNIX-LIKE operating systems do not require installation to a HDD, but instead can run from the BOOT media and simply utilize your systems hardware.</p>
                    <br>
                    <p>The following page provides links to resources and flavors of UNIX / UNIX-LIKE operating systems. Please refer to the manufacturers website for instructions on how to create media and how to boot for your system.</p>
                    <br><br><br><br>
                    <p>Did someone say <a href='http://en.wikipedia.org/wiki/Virtualization'>Virtualization</a>? <a href='https://www.virtualbox.org/'>VirtualBox</a> is the recommended cross platform tool. You are welcome to use others. Do note the videos may not play in MOST virtualized environments. Virtualization is the future.</p>
                </div>

            </div>
        </div>
        <div id='forward' class="link button bg-off-white hover-bg-white-smoke transition-5" data-current-page='media' data-next-page='media-select'>
            <i class="fa fa-linux"></i>
        </div>
    </div>
    <!-- end - media -->

