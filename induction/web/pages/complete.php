
    <!-- complete -->
    <div id='complete' class="page hidden-y transition-10">
        <div id='back' class="link button bg-green hover-bg-blue transition-5" data-current-page='complete' data-next-page='start'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-green">
            <div id='container'>

                <!-- complete - options -->
                <div id='complete'>
			<h3>Induction Complete!</h3>
			<p><i>By now you should have an entry level understanding of how to create, boot and use a UNIX/UNIX-LIKE operating system.</i></p>
			<p>This is only the beginning, it is up to you to develop your skills and become a true UNIX master!</p>
                </div>

            </div>
        </div>
        <div id='forward' class="G-green transition-5" data-current-page='complete' data-next-page='complete'>
        </div>
    </div>
    <!-- end - complete -->

