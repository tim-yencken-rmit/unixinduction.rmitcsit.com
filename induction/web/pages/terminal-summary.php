
    <!-- terminal-summary -->
    <div id='terminal-summary' class="page hidden-y transition-10 terminal-page">
        <div id='back' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-summary' data-next-page='terminal-remote-servers'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-ms-black">
            <div id='container'>

                <!-- terminal-summary - title -->
                <h1>Terminal</h1>

                <!-- terminal-summary - video -->
                <div id='video'>
                    <div class='terminal-video-container'>

                        <!-- terminal-summary - video - title -->
                        <h1>Summary</h1>
                        <hr>
                        <p>A summary of the CLI commands shown in this induction.</p>
                        <br>

                        <!-- terminal-summary - video -->
                        <video class='terminal-video' controls preload="none">
                            <source src="../web/assets/videos/Summary.mp4"  type="video/mp4">
                            <source src="../web/assets/videos/Summary.webm" type="video/webm">
                            Your browser does not support the video tag. Please update your browser.
                            We recommend Firefox.
                        </video>
                        <p>Issues playing? Download <a href='../web/assets/videos/Summary.mp4'>here</a></p>
                        <br><br>

                        <!-- terminal-summary - commands -->
                        <div class='terminal-video-commands'>
                            <h1>Commands</h1>
                            <hr>
                            <ul>
                                <li><a class="command" href='http://man.cx/cd'   >CD </a></li>
                                <li><a class="command" href='http://man.cx/ls'   >LS </a></li>
                                <li><a class="command" href='http://man.cx/pwd'  >PWD</a></li>
                                <li><a class="command" href='http://man.cx/man'  >MAN</a></li>
                                <li><a class="command" href='http://man.cx/mkdir'>MKDIR</a></li>
                                <li><a class="command" href='http://man.cx/rm'   >RM</a></li>
                                <li><a class="command" href='http://man.cx/mv'   >MV</a></li>
                                <li><a class="command" href='http://man.cx/cp'   >CP</a></li>
                                <li><a class="command" href='http://man.cx/vim'  >VIM</a></li>
                                <li><a class="command" href='http://man.cx/emacs'>EMACS</a></li>
                                <li><a class="command" href='http://man.cx/nedit'>NEDIT</a></li>
                                <li><a class="command" href='http://man.cx/chmod'>CHMOD</a></li>
                                <li><a class="command" href='http://man.cx/chown'>CHOWN</a></li>
                                <li><a class="command" href='http://man.cx/ps'   >PS</a></li>
                                <li><a class="command" href='http://man.cx/kill' >KILL</a></li>
                                <li><a class="command" href='http://man.cx/pkill'>PKILL</a></li>
                                <li><a class="command" href='http://man.cx/xkill'>XKILL</a></li>
                                <li><a class="command" href='http://man.cx/top'  >TOP</a></li>
                                <li><a class="command" href='http://man.cx/htop' >HTOP</a></li>
                                <li><a class="command" href='http://man.cx/sftp' >SFTP</a></li>
                                <li><a class="command" href='http://man.cx/ssh'  >SSH</a></li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div id='forward' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-summary' data-next-page='terminal-commands'>
            <i class="fa fa-linux"></i>
        </div>
    </div>
    <!-- end - terminal-summary -->

