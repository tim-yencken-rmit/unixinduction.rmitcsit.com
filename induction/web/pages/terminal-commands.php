
    <!-- terminal-commands -->
    <div id='terminal-commands' class="page hidden-y transition-10 terminal-page">
        <div id='back' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-commands' data-next-page='terminal-summary'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-ms-black">
            <div id='container'>

                <!-- terminal-commands - title -->
                <h1>Terminal</h1>

                <!-- terminal-commands - video -->
                <div id='video'>
                    <div class='terminal-video-container'>

                        <!-- terminal-commands - video - title -->
                        <h1>CLI Commands</h1>
                        <hr>
                        <p>There are thousands of commands, these are just a few. You have a lifetime to try them all!</p>
                        <br>

                        <!-- terminal-commands - commands -->
                        <div class='terminal-video-commands'>
                            <ul>
                                <li><a class="command" href='http://man.cx/apt'     >APT</a></li>
                                <li><a class="command" href='http://man.cx/awk'     >AWK</a></li>
                                <li><a class="command" href='http://man.cx/bzip2'   >BZIP2</a></li>
                                <li><a class="command" href='http://man.cx/cd'      >CD </a></li>
                                <li><a class="command" href='http://man.cx/chmod'   >CHMOD</a></li>
                                <li><a class="command" href='http://man.cx/chown'   >CHOWN</a></li>
                                <li><a class="command" href='http://man.cx/cp'      >CP</a></li>
                                <li><a class="command" href='http://man.cx/crontab' >CRONTAB</a></li>
                                <li><a class="command" href='http://man.cx/date'    >DATE</a></li>
                                <li><a class="command" href='http://man.cx/df'      >DF</a></li>
                                <li><a class="command" href='http://man.cx/diff'    >DIFF</a></li>
                                <li><a class="command" href='http://man.cx/dpkg'    >DPKG</a></li>
                                <li><a class="command" href='http://man.cx/emacs'   >EMACS</a></li>
                                <li><a class="command" href='http://man.cx/export'  >EXPORT</a></li>
                                <li><a class="command" href='http://man.cx/find'    >FIND</a></li>
                                <li><a class="command" href='http://man.cx/free'    >FREE</a></li>
                                <li><a class="command" href='http://man.cx/ftp'     >FTP</a></li>
                                <li><a class="command" href='http://man.cx/grep'    >GREP</a></li>
                                <li><a class="command" href='http://man.cx/gzip'    >GZIP</a></li>
                                <li><a class="command" href='http://man.cx/htop'    >HTOP</a></li>
                                <li><a class="command" href='http://man.cx/ifconfig'>IFCONFIG</a></li>
                                <li><a class="command" href='http://man.cx/kill'    >KILL</a></li>
                                <li><a class="command" href='http://man.cx/ls'      >LS </a></li>
                                <li><a class="command" href='http://man.cx/man'     >MAN</a></li>
                                <li><a class="command" href='http://man.cx/mkdir'   >MKDIR</a></li>
                                <li><a class="command" href='http://man.cx/mv'      >MV</a></li>
                                <li><a class="command" href='http://man.cx/mysql'   >MYSQL</a></li>
                                <li><a class="command" href='http://man.cx/nedit'   >NEDIT</a></li>
                                <li><a class="command" href='http://man.cx/passwd'  >PASSWD</a></li>
                                <li><a class="command" href='http://man.cx/ping'    >PING</a></li>
                                <li><a class="command" href='http://man.cx/pkill'   >PKILL</a></li>
                                <li><a class="command" href='http://man.cx/ps'      >PS</a></li>
                                <li><a class="command" href='http://man.cx/pwd'     >PWD</a></li>
                                <li><a class="command" href='http://man.cx/rm'      >RM</a></li>
                                <li><a class="command" href='http://man.cx/rpm'     >RPM</a></li>
                                <li><a class="command" href='http://man.cx/sed'     >SED</a></li>
                                <li><a class="command" href='http://man.cx/service' >SERVICE</a></li>
                                <li><a class="command" href='http://man.cx/sftp'    >SFTP</a></li>
                                <li><a class="command" href='http://man.cx/shutdown'>SHUTDOWN</a></li>
                                <li><a class="command" href='http://man.cx/sort'    >SORT</a></li>
                                <li><a class="command" href='http://man.cx/ssh'     >SSH</a></li>
                                <li><a class="command" href='http://man.cx/tar'     >TAR</a></li>
                                <li><a class="command" href='http://man.cx/top'     >TOP</a></li>
                                <li><a class="command" href='http://man.cx/uname'   >UNAME</a></li>
                                <li><a class="command" href='http://man.cx/unzip'   >UNZIP</a></li>
                                <li><a class="command" href='http://man.cx/vim'     >VIM</a></li>
                                <li><a class="command" href='http://man.cx/wget'    >WGET</a></li>
                                <li><a class="command" href='http://man.cx/less'    >LESS</a></li>
                                <li><a class="command" href='http://man.cx/su'      >SU</a></li>
                                <li><a class="command" href='http://man.cx/tail'    >TAIL</a></li>
                                <li><a class="command" href='http://man.cx/whatis'  >WHATIS</a></li>
                                <li><a class="command" href='http://man.cx/xargs'   >XARGS</a></li>
                                <li><a class="command" href='http://man.cx/xkill'   >XKILL</a></li>
                                <li><a class="command" href='http://man.cx/yum'     >YUM</a></li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div id='forward' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-commands' data-next-page='complete'>
            <i class="fa fa-linux"></i>
        </div>
    </div>
    <!-- end - terminal-commands -->

