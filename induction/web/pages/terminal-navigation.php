
    <!-- terminal-navigation -->
    <div id='terminal-navigation' class="page hidden-y transition-10 terminal-page">
        <div id='back' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-navigation' data-next-page='terminal-cli-vs-gui'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-ms-black">
            <div id='container'>

                <!-- terminal-navigation - title -->
                <h1>Terminal</h1>

                <!-- terminal-navigation - video -->
                <div id='video'>
                    <div class='terminal-video-container'>

                        <!-- terminal-navigation - video - title -->
                        <h1>Navigation</h1>
                        <hr>
                        <p>An introduction to CLI tools that allow for easy navigation of the filesystem.</p>
                        <br>

                        <!-- terminal-navigation - video -->
                        <video class='terminal-video' controls preload="none">
                            <source src="../web/assets/videos/Navigation.mp4"  type="video/mp4">
                            <source src="../web/assets/videos/Navigation.webm" type="video/webm">
                            Your browser does not support the video tag. Please update your browser.
                            We recommend Firefox.
                        </video>
                        <p>Issues playing? Download <a href='../web/assets/videos/Navigation.mp4'>here</a></p>
                        <br><br>

                        <!-- terminal-navigation - commands -->
                        <div class='terminal-video-commands'>
                            <h1>Commands</h1>
                            <hr>
                            <ul>
                                <li><a class="command" href='http://man.cx/cd' >CD </a></li>
                                <li><a class="command" href='http://man.cx/ls' >LS </a></li>
                                <li><a class="command" href='http://man.cx/pwd'>PWD</a></li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div id='forward' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-navigation' data-next-page='terminal-file-manipulation'>
            <i class="fa fa-linux"></i>
        </div>
    </div>
    <!-- end - terminal-navigation -->

