
    <!-- terminal -->
    <div id='terminal' class="page hidden-y transition-10">
        <div id='back' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal' data-next-page='start'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-ms-black">
            <div id='container'>

                <!-- terminal - title -->
                <h1>Terminal</h1>
                <span><i>The best tool ever!.</i></span>

                <!-- terminal - spacer -->
                <br><br><br><br>

                <!-- terminal - intro -->
                <p>A terminal provides access to a shell.</p>
                <p>A shell interprets text based commands and presents text based output.</p> 
                <p>Mastering terminal is essential for any IT graduate.</p>

                <!-- terminal - image -->
                <div id='image'>
                <img src='../web/assets/img/terminal.png' alt='terminal' />
                    <br>
                    <p>"An example terminal emulator from Fedora 21"</p>
                </div>

                <!-- terminal - spacer -->
                <br><br><br><br>

                <!-- terminal - intro -->
                <p>In the next section, we will introduce you to basic terminal commands through a series of instructional videos.</p>
                <p>There are also many many resources online and it is encouraged to practice as much as possible.</p>
                <p>Anything you can currently do on your computer can be accomplished faster using terminal.</p>

                <!-- terminal - spacer -->
                <br><br><br><br>

                <!-- terminal - intro -->
                <p>If you were able to create & boot a UNIX/UNIX-LIKE/Linux operating system it is encouraged to perform the commands w/ the videos.</p>
                <p>The best way to learn something new is to practice and make mistakes.</p>
                <p>The more mistakes you make the faster you learn.</p>

                <!-- terminal - spacer -->
                <br><br><br><br>

            </div>
        </div>
        <div id='forward' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal' data-next-page='terminal-cli-vs-gui'>
            <i class="fa fa-linux"></i>
        </div>
    </div>
    <!-- end - terminal -->

