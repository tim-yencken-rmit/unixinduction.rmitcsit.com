
    <!-- terminal-cli-vs-gui -->
    <div id='terminal-cli-vs-gui' class="page hidden-y transition-10 terminal-page">
        <div id='back' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-cli-vs-gui' data-next-page='start'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-ms-black">
            <div id='container'>

                <!-- terminal-cli-vs-gui - title -->
                <h1>Terminal</h1>

                <!-- terminal-cli-vs-gui - video -->
                <div id='video'>
                    <div class='terminal-video-container'>
                        <h1>CLI vs GUI</h1>
                        <hr>
                        <p>An overview of the main differences between "Command Line Interfaces (CLI)" and "Graphical User Interfaces (GUI)"</p>
                        <br><br>
                        <video class='terminal-video' controls preload="none">
                            <source src="../web/assets/videos/CLI vs GUI.mp4"  type="video/mp4">
                            <source src="../web/assets/videos/CLI vs GUI.webm" type="video/webm">
                            Your browser does not support the video tag. Please update your browser.
                            We recommend Firefox.
                        </video>
                        <p>Issues playing? Download <a href='../web/assets/videos/CLI vs GUI.mp4'>here</a></p>
                    </div>
                </div>

            </div>
        </div>
        <div id='forward' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-cli-vs-gui' data-next-page='terminal-navigation'>
            <i class="fa fa-linux"></i>
        </div>
    </div>
    <!-- end - terminal-cli-vs-gui -->

