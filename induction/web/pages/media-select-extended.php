
    <!-- media-select-extended -->
    <div id='media-select-extended' class="page hidden-y transition-10">
        <div id='back' class="link button bg-white-smoke hover-bg-off-white transition-5" data-current-page='media-select-extended' data-next-page='media-select'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-white-smoke">
            <div id='container'>

                <!-- media-select-extended - title -->
                <h1>Extended Documentation</h1>
		<span><i>For those unable to create & boot a UNIX or UNIX-LIKE operating system.</i></span>


                <!-- media-select-extended -->
                <div id='distros'>

                    <!-- media-select-extended - TOOLS -->
                    <h1>Useful Tools</h1>
                    <ul>
                        <li>
                            <a href='https://rufus.akeo.ie/'>Rufus</a>
                            <p class='description'>A very well made Windows compatible tool that provides an easy to use way to create bootable USBs from UNIX/UNIX-LIKE/Linux ISOs.</p>
                        </li>
                        <li>
                            <a href='http://unetbootin.sourceforge.net/'>Unetbootin</a>
                            <p class='description'>"UNetbootin allows you to create bootable Live USB drives for Ubuntu, Fedora, and other Linux distributions without burning a CD. It runs on Windows, Linux, and Mac OS X. You can either let UNetbootin download one of the many distributions supported out-of-the-box for you, or supply your own Linux .iso file if you've already downloaded one or your preferred distribution isn't on the list." unetbootin.sourceforge.net</p>
                        </li>
                    </ul>

		    <!-- spacers -->
		    <br><br><br>

                    <!-- media-select-extended - GUIDES -->
		    <h1>Generic PDF Guides</h1>

			    <!-- guides - linux -->
			    <h2>Linux</h2>
			    <ul>
				<li>
				    <a href='../web/assets/pdf/linux-create.pdf'>How to Create</a>
				    <p class='description'>A PDF providing instructions on how to create a bootable USB in a Linux enviroment.</p>
				</li>
				<li>
				    <a href='../web/assets/pdf/linux-boot.pdf'>How to Boot</a>
				    <p class='description'>A PDF providing instructions on how to boot a bootable USB on a standard PC.</p>
				</li>
			    </ul>

			    <!-- guides - mac osx -->
			    <h2>MAC / OSX</h2>
			    <ul>
				<li>
				    <a href='../web/assets/pdf/mac-create.pdf'>How to Create</a>
				    <p class='description'>A PDF providing instructions on how to create a bootable USB in an OSX enviroment.</p>
				</li>
				<li>
				    <a href='../web/assets/pdf/mac-boot.pdf'>How to Boot</a>
				    <p class='description'>A PDF providing instructions on how to boot a bootable USB on a MAC.</p>
				</li>
				<li>
				    <p>If you are unable to get a working bootable USB in OSX you can use the "Terminal" available VIA spotlight to complete the rest of the induction</p>
				</li>
			    </ul>

			    <!-- guides - windows -->
			    <h2>Windows</h2>
			    <ul>
				<li>
				    <a href='../web/assets/pdf/windows-create.pdf'>How to Create</a>
				    <p class='description'>A PDF providing instructions on how to create a bootable USB in a Windows enviroment.</p>
				</li>
				<li>
				    <a href='../web/assets/pdf/windows-boot.pdf'>How to Boot</a>
				    <p class='description'>A PDF providing instructions on how to boot a bootable USB on a standard PC.</p>
				</li>
			    </ul>

                    <!-- end - media-select-extended - GUIDES -->

		    <!-- spacers -->
		    <br><br><br>

		</div>
            </div>
        </div>
        <div id='forward' class="link button bg-white-smoke hover-bg-green transition-5" data-current-page='media-select-extended' data-next-page='start'>
	    <h3>Back to Start</h3>
        </div> 
    </div>
    <!-- end - media-select-extended -->

