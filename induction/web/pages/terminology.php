

    <!-- terminology -->
    <div id='terminology' class="page hidden-y transition-10">

        <!-- terminology - back -->
        <div id='back' class="link button bg-blue hover-bg-red transition-5" data-current-page='terminology' data-next-page='intro'>
            <i class="fa fa-sort-up"></i>
        </div> 

        <!-- terminology - content -->
        <div id='content' class="bg-blue">
            <div id='terminology'>

                <!-- terminology - icon -->
                <h1><i class="fa fa-list"></i></h1>

                <!-- terminology - title -->
                <h2>Terminology</h2>

                <!-- terminology -->

                    <!-- COMPUTER TERIMANL -->
                    <div class="terminology">
                        <h3>Computer Terminal</h3>
                        <p>"A computer terminal is an electronic or electromechanical hardware device that is used for entering data into, and displaying data from, a computer or a computing system. Early terminals were inexpensive devices but very slow compared to punched cards or paper tape for input, but as the technology improved and video displays were introduced, terminals pushed these older forms of interaction from the industry. A related development was timesharing systems, which evolved in parallel and made up for any inefficiencies of the user's typing ability with the ability to support multiple users on the same machine, each at their own terminal."</p>
                        <a href="http://en.wikipedia.org/wiki/Computer_terminal">http://en.wikipedia.org/wiki/Computer_terminal</a>
                    </div>

                    <!-- Live CD / DVD / USB -->
                    <div class='terminology'>
                        <h3>Live CD / DVD / USB</h3>
                        <p>"A live CD, live DVD, or live disc is a complete bootable computer operating system which runs in the computer's memory, rather than loading from the hard disk drive. It allows users to experience and evaluate an operating system without installing it or making any changes to the existing operating system on the computer."</p>
                        <a href="http://en.wikipedia.org/wiki/Live_CD">http://en.wikipedia.org/wiki/Live_CD</a>
                    </div>

                    <!-- Linux -->
                    <div class='terminology'>
                        <h3>Linux</h3>
                        <p>"Linux is a Unix-like computer operating system assembled under the model of free and open source software development and distribution. The defining component of Linux is the Linux kernel,[11] an operating system kernel first released on 5 October 1991, by Linus Torvalds.[12][13] Since the C compiler that builds Linux and the main supporting user space system tools and libraries originated in the GNU Project, initiated in 1983 by Richard Stallman, the Free Software Foundation prefers the name GNU/Linux.[14][15]"</p>
                        <a href="http://en.wikipedia.org/wiki/Linux">http://en.wikipedia.org/wiki/Linux</a>
                    </div>

                    <!-- UNIX -->
                    <div class='terminology'>
                        <h3>UNIX</h3>
                        <p>"Unix (officially trademarked as UNIX, sometimes also written as UNIX in small caps) is a multitasking, multi-user computer operating system originally developed in 1969 by a group of AT&amp;T employees at Bell Labs, including Ken Thompson, Dennis Ritchie, Brian Kernighan, Douglas McIlroy, Michael Lesk and Joe Ossanna.[1]"</p>
                        <a href="http://en.wikipedia.org/wiki/Unix">http://en.wikipedia.org/wiki/Unix</a>
                    </div>

                    <!-- UNIX-LIKE -->
                    <div class='terminology'>
                        <h3>UNIX-LIKE</h3>
                        <p>A Unix-like (sometimes referred to as UN*X or *nix) operating system is one that behaves in a manner similar to a Unix system, while not necessarily conforming to or being certified to any version of the Single UNIX Specification.</p>
                        <a href="http://en.wikipedia.org/wiki/Unix-like">http://en.wikipedia.org/wiki/Unix-like</a>
                    </div>

                    <!-- UNIX-SHELL -->
                    <div class='terminology'>
                        <h3>SHELL</h3>
                        <p>"A Unix shell is a command-line interpreter or shell that provides a traditional user interface for the Unix operating system and for Unix-like systems. Users direct the operation of the computer by entering commands as text for a command line interpreter to execute, or by creating text scripts of one or more such commands. Users typically interact with a Unix shell using a terminal emulator, however, direct operation via serial hardware connections, or networking session, are common for server systems."</p>
                        <a href="http://en.wikipedia.org/wiki/Unix_shell">http://en.wikipedia.org/wiki/Unix_shell</a>
                    </div>

                    <!-- TERMINAL EMULATOR -->
                    <div class='terminology'>
                        <h3>Terminal Emulator</h3>
                        <p>"A terminal emulator, terminal application, term, or tty for short, is a program that emulates a video terminal within some other display architecture. Though typically synonymous with a shell or text terminal, the term terminal covers all remote terminals, including graphical interfaces. A terminal emulator inside a graphical user interface is often called a terminal window."</p>
                        <a href="http://en.wikipedia.org/wiki/Terminal_emulator">http://en.wikipedia.org/wiki/Terminal_emulator</a>
                    </div>

                <!-- end - terminology -->

            </div>
        </div>

        <!-- terminology - forward -->
        <div id='forward' class="link button bg-blue hover-bg-off-white transition-5" data-current-page='terminology' data-next-page='overview'>
            <i class="fa fa-info"></i>
        </div>

    </div>
    <!-- end terminology -->


