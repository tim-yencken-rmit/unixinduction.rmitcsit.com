
    <!-- overview -->
    <div id='overview' class="page hidden-y transition-10">
        <div id='back' class="link button bg-off-white hover-bg-blue transition-5" data-current-page='overview' data-next-page='terminology'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-off-white">
            <div id='container'>

                <!-- overview - title -->
                <h1>Overview</h1>

                <!-- overview - info -->
                <div id='info'>
                    <br>
                    <p>This induction is comprised of two main parts.</p>
                    <ul>
                        <li><a href='http://en.wikipedia.org/wiki/Download'>Downloading</a>, Creating & <a href='http://en.wikipedia.org/wiki/Booting'>Booting</a> of your <a href='http://en.wikipedia.org/wiki/Unix'>UNIX</a> / <a href='http://en.wikipedia.org/wiki/Unix-like'>UNIX-LIKE</a> operating system.</li> 
                        <li>Understanding <a href='http://en.wikipedia.org/wiki/Terminal_emulator'>Terminal</a>: basic commands and usage.</li>
                    </ul>
                    <br>
                    <p>It is important that you do your best to understand the material presented to you.</p>
                    <br>
                    <p>The intended use is to setup your UNIX / UNIX-LIKE operating system and use it as a platform to test and practice the TERMINAL commands you learn in the following section.</p>
                    <br>
                    <p>In the event that you are unable to complete either section(s), do as much as you can.<p>
                    <p>You can watch the instructional videos without having your test environment setup.</p>
                    <br>
                    <p>This induction is a starting point, it will not show you every operating system, or teach you every command.</p>
                    <p>Its goal is simple, get you started. The rest is up to you!</p>
                    <p>
                    <br><br><br>
                    <p>Pro Tip: There are multiple <a href='../../start/'>inductions</a> available, remember to try them all.</p>
                </div>

            </div>
        </div>
        <div id='forward' class="link button bg-off-white hover-bg-green transition-5" data-current-page='overview' data-next-page='start'>
            <h3>Start</h3>
        </div>
    </div>
    <!-- end overview -->

