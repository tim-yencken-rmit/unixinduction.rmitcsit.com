
    <!-- media-select -->
    <div id='media-select' class="page hidden-y transition-10">
        <div id='back' class="link button bg-white-smoke hover-bg-off-white transition-5" data-current-page='media-select' data-next-page='media'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-white-smoke">
            <div id='container'>

                <!-- media-select - title -->
                <h1>Select your desired UNIX / UNIX-LIKE operating system.</h1>

                <!-- media-select - instructions -->
                <br>
                <p><i>Each distribution is given a specific score, the lower the score, the easier it is to use.</i></p>
                <p>The higher the score,the more internet points you gain for using it.</p>
                <p><i>Remember to try as many different flavors of UNIX / LINUX as possible.</i></p>
		<br>
                <p>Feel a little lost? Try Ubuntu Desktop!</p>

                <!-- media-select -->
                <div id='distros'>

                    <!-- media-select - UNIX -->
                    <h2>UNIX</h2>
                    <ul>
                        <li>
                            <a href='http://www.freebsd.org/'>FreeBSD</a>
                            <p class='description'>"FreeBSD is an advanced computer operating system used to power modern servers, desktops and embedded platforms. A large community has continually developed it for more than thirty years. Its advanced networking, security and storage features have made FreeBSD the platform of choice for many of the busiest web sites and most pervasive embedded networking and storage devices." -- FreeBSD.org</p>
                            <p class='rating'>Difficulty: 7</p>
                        </li>
                    </ul>

                    <!-- media-select - UNIX -->
                    <h2>UNIX LIKE</h2>
                    <ul>
                        <li>
                            <a href='http://www.openbsd.org/'>OpenBSD</a>
                            <p class='description'>"The OpenBSD project produces a FREE, multi-platform 4.4BSD-based UNIX-like operating system. Our efforts emphasize portability, standardization, correctness, proactive security and integrated cryptography. As an example of the effect OpenBSD has, the popular OpenSSH software comes from OpenBSD." -- OpenBSD.org</p>
                            <p class='rating'>Difficulty: 8</p>
                        </li>
                    </ul>

                    <!-- media-select - UNIX -->
                    <h2>LINUX</h2>
                    <ul>
                        <li>
                            <a href='https://www.archlinux.org/'>Arch Linux</a>
                            <p class='description'>Recommended for those who want to learn! Arch will push you to the next level with its excellent documentation and experimental pacakages.</p>
                            <p class='rating'>Difficulty: 9</p>
                            <p class='extra'>Arch is my distribution of choice. When a package fails, I learn.</a>
                        </li>
                        <li>
                            <a  href='https://www.debian.org/'>Debian</a>
                            <p class='description'>"Debian is a free operating system (OS) for your computer. An operating system is the set of basic programs and utilities that make your computer run." -- Debian.org</p>
                            <p class='rating'>Difficulty: 4</p>
                            <p class='recommended'>Recommended for beginners, who love stability!</p>
                        </li>
                        <li>
                            <a  href='https://getfedora.org/'>Fedora</a>
                            <p class='description'>"Less setup, more innovation. Choose a flavor of Fedora streamlined for your needs, and get to work right away." -- getfedora.org</p>
                            <p class='rating'></p>
                        </li>
                        <li>
                            <a  href='http://www.linuxmint.com/'>Linux MINT</a>
                            <p class='description'>A friendly easy to use LINUX distribution that offers wide compatibility.</p>
                            <p class='rating'>Difficulty: 2</p>
                            <p class='recommended'>Recommended for beginners, who dont like Ubuntu.</p>
                        </li>
                        <li>
                            <a  href='../web/assets/linux/linux-mint-rmit-extractme.zip'>Linux MINT [ RMIT Customized ]</a>
                            <p class='description'>A customized version of LINUX MINT that contains the videos from this induction on the disk.</p>
                            <p class='rating'>Difficulty: 1</p>
                            <p class='recommended'>Recommended for beginners, who want the all in one.</p>
                        </li>
                        <li>
                            <a  href='http://www.redhat.com/en'>RedHat</a>
                            <p class='description'>A server grade distribution for the more technically minded.</p>
                            <p class='rating'>Difficulty: 7</p>
                        </li>
                        <li>
                            <a  href='http://www.ubuntu.com/'>Ubuntu</a>
                            <p class='description'>Its so compatible it will run on your smartphone!</p>
                            <p class='rating'>Difficulty: 1</p>
                            <p class='recommended'>Recommended for beginners, who love purple!</p>
                        </li>
                    </ul>

                </div>

                <!-- media-select - instructions  -->
                <h2>To create your boot media please refer to the manufacturers website.</h2>
                <h4>Ubuntu has the best documentation for <a href='http://www.ubuntu.com/download/desktop/create-a-usb-stick-on-windows'>Windows</a> & <a href='http://www.ubuntu.com/download/desktop/create-a-usb-stick-on-mac-osx'>MAC OSX</a>, it can be used with any of the above flavors.</h4>
                <br>
                <br>

            </div>
        </div>
        <div id='forward' class="link button bg-white-smoke hover-bg-green transition-5" data-current-page='media-select' data-next-page='start'>
            <h3>Back to Start</h3>
        </div>
        <div id='forward' class="link button bg-white-smoke hover-bg-green transition-5" data-current-page='media-select' data-next-page='media-select-extended'>
            <h3>Extended Create & Boot Documentation</h3>
        </div>
    </div>
    <!-- end - media-select -->

