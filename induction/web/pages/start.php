
    <!-- start -->
    <div id='start' class="page hidden-y transition-10">
        <div id='back' class="link button bg-green hover-bg-blue transition-5" data-current-page='start' data-next-page='overview'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-green">
            <div id='container'>

                <!-- start - options -->
                <div id='options'>
                    <div id='forward' class="link button bg-green hover-bg-green transition-5" data-current-page='start' data-next-page='media'>
                        <h1>Getting **nix</h1>
			<p>Downloading, creating and booting of your UNIX/UNIX-LIKE/Linux operating system.</p>
                    </div>
                    <div id='forward' class="link button bg-green hover-bg-green transition-5" data-current-page='start' data-next-page='terminal'>
                        <h1>Terminal</h1>
			<p>Base commands and example usage.</p>
                    </div>
                </div>

            </div>
        </div>
        <div id='forward' class="bg-green transition-5" data-current-page='start' data-next-page='start'>
        </div>
    </div>
    <!-- end - start -->

