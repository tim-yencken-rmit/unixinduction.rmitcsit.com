
    <!-- terminal-file-manipulation -->
    <div id='terminal-file-manipulation' class="page hidden-y transition-10 terminal-page">
        <div id='back' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-file-manipulation' data-next-page='terminal-navigation'>
            <i class="fa fa-sort-up"></i>
        </div> 
        <div id='content' class="bg-ms-black">
            <div id='container'>

                <!-- terminal-file-manipulation - title -->
                <h1>Terminal</h1>

                <!-- terminal-file-manipulation - video -->
                <div id='video'>
                    <div class='terminal-video-container'>

                        <!-- terminal-file-manipulation - video - title -->
                        <h1>File Manipulation</h1>
                        <hr>
                        <p>An introduction to CLI tools that assist with moving, copying, deleting and other various file related tasks.</p>
                        <br>

                        <!-- terminal-file-manipulation - video -->
                        <video class='terminal-video' controls preload="none">
                            <source src="../web/assets/videos/File Manipulation.mp4"  type="video/mp4">
                            <source src="../web/assets/videos/File Manipulation.webm" type="video/webm">
                            Your browser does not support the video tag. Please update your browser.
                            We recommend Firefox.
                        </video>
                        <p>Issues playing? Download <a href='../web/assets/videos/File Manipulation.mp4'>here</a></p>
                        <br><br>

                        <!-- terminal-file-manipulation - commands -->
                        <div class='terminal-video-commands'>
                            <h1>Commands</h1>
                            <hr>
                            <ul>
                                <li><a class="command" href='http://man.cx/cd'   >CD </a></li>
                                <li><a class="command" href='http://man.cx/ls'   >LS </a></li>
                                <li><a class="command" href='http://man.cx/pwd'  >PWD</a></li>
                                <li><a class="command" href='http://man.cx/man'  >MAN</a></li>
                                <li><a class="command" href='http://man.cx/mkdir'>MKDIR</a></li>
                                <li><a class="command" href='http://man.cx/rm'   >RM</a></li>
                                <li><a class="command" href='http://man.cx/mv'   >MV</a></li>
                                <li><a class="command" href='http://man.cx/cp'   >CP</a></li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div id='forward' class="link button bg-ms-black hover-bg-green transition-5" data-current-page='terminal-file-manipulation' data-next-page='terminal-file-editors'>
            <i class="fa fa-linux"></i>
        </div>
    </div>
    <!-- end - terminal-file-manipulation -->

