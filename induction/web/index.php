<?php

    /** page **/
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    } else { $page = 'welcome'; }

?>
<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Unix Induction</title>
        <meta content="no-cache">
        <meta name="description" content="RMIT University CSIT html5 unix induction." />
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
        <link rel="shortcut icon" href="favicon.ico">
        <!-- 3rd Party -->

            <!-- Font Awesome -->
            <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">

            <!-- Google Fonts -->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

            <!-- jQuery -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

        <!-- END -->
        <script src="jquery/toggles.js"></script>
        <script>
            $(document).ready(function() {
                $("#<?php echo $page; ?>").toggleClass("hidden-y");
            });
        </script>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body class="bg-red">
        <div id="c">

            <!-- pages -->
            <?php require_once('pages/welcome.php');            ?>
            <?php require_once('pages/intro.php');              ?>
            <?php require_once('pages/terminology.php');        ?>
            <?php require_once('pages/overview.php');           ?>
            <?php require_once('pages/start.php');              ?>

                <!-- pages - media -->
                <?php require_once('pages/media.php');                  ?>
                <?php require_once('pages/media-select.php');           ?>
                <?php require_once('pages/media-select-extended.php');  ?>

                <!-- pages - terminal -->
                <?php require_once('pages/terminal.php');                   ?>
                <?php require_once('pages/terminal-cli-vs-gui.php');        ?>
                <?php require_once('pages/terminal-navigation.php');        ?>
                <?php require_once('pages/terminal-file-manipulation.php'); ?>
                <?php require_once('pages/terminal-file-editors.php');      ?>
                <?php require_once('pages/terminal-permissions.php');       ?>
                <?php require_once('pages/terminal-process-control.php');   ?>
                <?php require_once('pages/terminal-remote-servers.php');    ?>
                <?php require_once('pages/terminal-summary.php');           ?>
                <?php require_once('pages/terminal-commands.php');          ?>

            <!-- pages -->
            <?php require_once('pages/complete.php');           ?>

        <div>
    </body>
</html>

